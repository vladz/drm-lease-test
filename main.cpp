#include <cstring>
#include <iostream>
#include <vector>

#include "wayland-drm-lease-v1-client-protocol.h"

struct lease_device_t;

struct lease_state_t
{
    uint32_t connectorId = 0;
    std::vector<lease_device_t *> devices;
};

struct lease_connector_t
{
    lease_device_t *device;
    wp_drm_lease_connector_v1 *connector;
    uint32_t id = 0;
};

struct lease_device_t
{
    lease_state_t *state;
    int drm_fd = -1;
    wp_drm_lease_device_v1 *device = nullptr;
    std::vector<lease_connector_t *> connectors;
};

static void
lease_fd(void *data, wp_drm_lease_v1 *lease, int32_t leased_fd)
{
    fprintf(stdout, "lease fd (%d)\n", leased_fd);
}

static void
lease_finished(void *data, wp_drm_lease_v1 *lease)
{
    fprintf(stdout, "lease finished");
}

static const wp_drm_lease_v1_listener lease_listener = {
    .lease_fd = lease_fd,
    .finished = lease_finished,
};

static void
lease_connector_name(void *data, wp_drm_lease_connector_v1 *conn, const char *name)
{
    fprintf(stdout, "connector name (%s)\n", name);
}

static void
lease_connector_desc(void *data, wp_drm_lease_connector_v1 *conn, const char *desc)
{
    fprintf(stdout, "connector desc (%s)\n", desc);
}

static void
lease_connector_id(void *data, wp_drm_lease_connector_v1 *conn, uint32_t id)
{
    fprintf(stdout, "connector id (%d)\n", id);
    lease_connector_t *lease_connector = static_cast<lease_connector_t *>(data);
    lease_connector->id = id;
}

static void lease_connector_done(void *data, wp_drm_lease_connector_v1 *conn)
{
    fprintf(stdout, "connector done\n");
}

static void
lease_connector_withdrawn(void *data, wp_drm_lease_connector_v1 *conn)
{
    fprintf(stdout, "connector withdrawn\n");
}

static const wp_drm_lease_connector_v1_listener lease_connector_listener = {
    .name = lease_connector_name,
    .description = lease_connector_desc,
    .connector_id = lease_connector_id,
    .done = lease_connector_done,
    .withdrawn = lease_connector_withdrawn,
};

static void
lease_device_drm_fd(void *data, wp_drm_lease_device_v1 *device, int32_t fd)
{
    fprintf(stdout, "got drm_fd (%d)\n", fd);
    lease_device_t *lease_device = static_cast<lease_device_t *>(data);
    lease_device->drm_fd = fd;
}

static void
lease_device_connector(void *data, wp_drm_lease_device_v1 *device, wp_drm_lease_connector_v1 *conn)
{
    fprintf(stdout, "connector\n");
    lease_device_t *lease_device = static_cast<lease_device_t *>(data);

    lease_connector_t *lease_connector = new lease_connector_t;
    lease_connector->device = lease_device;
    lease_connector->connector = conn;
    lease_device->connectors.push_back(lease_connector);

    wp_drm_lease_connector_v1_add_listener(conn, &lease_connector_listener, lease_connector);
}

static void
lease_device_done(void *data, wp_drm_lease_device_v1 *dev)
{
    fprintf(stdout, "done\n");
}

static void
lease_device_released(void *data, wp_drm_lease_device_v1 *dev)
{
    fprintf(stdout, "released\n");
}

static const wp_drm_lease_device_v1_listener lease_device_listener = {
    .drm_fd = lease_device_drm_fd,
    .connector = lease_device_connector,
    .done = lease_device_done,
    .released = lease_device_released,
};

static void
registry_global(void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
    lease_state_t *state = static_cast<lease_state_t *>(data);

    if (strcmp(interface, wp_drm_lease_device_v1_interface.name) == 0) {
        fprintf(stdout, "%s (%ud) version %d available\n", interface, version, name);

        lease_device_t *device = new lease_device_t;
        device->state = state;
        state->devices.push_back(device);

        device->device = static_cast<wp_drm_lease_device_v1 *>(wl_registry_bind(registry, name, &wp_drm_lease_device_v1_interface, std::min(version, 1u)));
        wp_drm_lease_device_v1_add_listener(device->device, &lease_device_listener, device);
    }
}

static void
registry_global_remove(void *data, wl_registry *wl_registry, uint32_t name)
{
    fprintf(stdout, "global (%d) has been removed\n", name);
}

static const wl_registry_listener registry_listener = {
    .global = registry_global,
    .global_remove = registry_global_remove,
};

int
main()
{
    wl_display *display = wl_display_connect(nullptr);
    if (!display) {
        return -1;
    }

    lease_state_t state;
    wl_registry *registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener, &state);
    wl_display_dispatch(display);
    wl_display_roundtrip(display);

    if (state.devices.empty()) {
        fprintf(stderr, "no lease devices\n");
        return -1;
    }

    lease_device_t *lease_device = state.devices.front();
    if (lease_device->connectors.empty()) {
        fprintf(stderr, "no lease connectors\n");
        return -1;
    }

    lease_connector_t *lease_connector = lease_device->connectors.front();
    fprintf(stderr, "attempting to lease connector (%d)", lease_connector->id);

    wp_drm_lease_request_v1 *request = wp_drm_lease_device_v1_create_lease_request(lease_device->device);
    wp_drm_lease_request_v1_request_connector(request, lease_connector->connector);
    wp_drm_lease_v1 *lease = wp_drm_lease_request_v1_submit(request);

    wp_drm_lease_v1_add_listener(lease, &lease_listener, nullptr);
    wl_display_dispatch(display);
    wl_display_roundtrip(display);

    return 0;
}
